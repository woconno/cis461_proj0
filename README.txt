NOTE 1: I was absent in class last thursday because I had the flu, it is likely that further specifications were given in class that I missed.
NOTE 2: I'm new to C++ (I really only use C) and I couldn't get the lexer to play nice with file descriptors,
so you still have to pipe from stdin, which is not in spec.

BUILD INSTRUCTIONS (on my machine interacting with docker requires sudo, so use that if need be)

while in the "compilers_reflex_1" folder:
1. Put your test file in the "data" folder (test.txt is provided in there already)
2. run "make image"
3. run "docker run -it proj0_woconnor"
You should now be in a shell in the docker container
4. run "./bin/to_html_quotes < ./data/test.txt"
Substitute test.txt with whatever the name of your test file is.
Notice that the file is piped in via stdin rather than provided as an argument.
