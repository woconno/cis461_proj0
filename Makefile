
all:
	echo "Building in src, executable goes in bin"
	(cd src; make lexer;)

image:
	(cd src; make clean;)
	docker build --tag=proj0_woconnor .